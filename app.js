const express = require('express');
const cors = require('cors');
const config = require('./config');
const mongoose = require('mongoose');
const morgan = require('morgan');

const apiItemsRouter = require('./routes/api.items.shopping');

const app = express();

/**
 * Connect to database by config file
 */
mongoose.connect(config.database, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log("Connected to the database");
    }
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan('dev'));

app.use('/api/items', apiItemsRouter);

app.listen(config.port, () => {
    console.log('App is running on ' + config.port);
});