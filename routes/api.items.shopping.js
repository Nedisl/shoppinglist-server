const express = require('express');
const router = express.Router();

const dbItemsShopping = require('../utils/db/db.items.shopping');

/* GET all available shopping items. */
router.get('/list', async (req, res) => {
  let data = await dbItemsShopping.getItems();
  return res.status(data['meta'].code).send(data);
});

/* POST item into DB. */
router.post('/add', async (req, res) => {
    let data = await dbItemsShopping.addItem(req.body);
    return res.status(data['meta'].code).send(data);
});

/* DELETE item from DB. */
router.delete('/delete', async (req, res) => {
    let data = await dbItemsShopping.deleteItem(req.body);
    return res.status(data['meta'].code).send(data);
});

module.exports = router;
