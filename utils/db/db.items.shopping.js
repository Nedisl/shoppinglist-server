const Item = require('../../models/item');

module.exports = {
    /**
     * Get all available items from db
     * @returns {Response<Object>}
     */
    getItems: async () => {
        let items = await Item.find().exec();
        return {
            meta: {
                "success": true,
                "code": 200,
                "message": "Items successfully received"
            },
            data: {
                items: items
            }
        };
    },

    /**
     * Put item into db
     * @param userItem
     * @returns {Response<Object>}
     */
    addItem: async userItem => {
        let existingItem = await Item.findOne().where("name").in([userItem.name]).exec();
        if (existingItem) {
            return {
                meta: {
                    "success": false,
                    "code": 200,
                    "message": "Item with such name already exists"
                },
                data: null
            };
        } else {
            let item = new Item();

            if (!userItem.name || userItem.name === '') {
                return {
                    meta: {
                        "success": false,
                        "code": 200,
                        "message": "Item name shouldn't be empty"
                    },
                    data: null
                };
            } else {
                item.name = userItem.name;
            }

            if (userItem.description) item.description = userItem.description;
            if (userItem.price) item.price = userItem.price;

            item.save();

            return {
                meta: {
                    "success": true,
                    "code": 200,
                    "message": "Item successfully added"
                },
                data: {
                    item: item
                }
            };
        }
    },

    /**
     * Delete item
     * @param userItem
     * @returns {Response<Object>}
     */
    deleteItem: async userItem => {
        let item = await Item.remove({_id: userItem._id}).exec();

        if (!item.n) {
            return {
                meta: {
                    "success": false,
                    "code": 404,
                    "message": "Item not found"
                },
                data: null
            }
        }

        return {
            meta: {
                "success": true,
                "code": 200,
                "message": "Item successfully removed"
            },
            data: userItem
        };
    }
};