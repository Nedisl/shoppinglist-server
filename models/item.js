const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Item Schema for Shopping list
 */
const ItemSchema = new Schema({
    name: {
        type: String,
        unique: true
    },
    description: {
        type: String,
        default: ''
    },
    price: {
        type: Number,
        default: 0
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Item', ItemSchema);